'use client'
import React, { Suspense } from "react"
import { SessionProvider } from "next-auth/react"
const Layout = (p: any) => {
    return (
        <Suspense>
            <SessionProvider>
                {p.children}
            </SessionProvider>
        </Suspense>
    )
}

export default Layout
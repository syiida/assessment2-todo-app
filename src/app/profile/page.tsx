'use client'
import Navbar from "@/components/navbar";
import { useSession } from "next-auth/react";


export default function Page() {
    const { data: session }: any = useSession()
    return (
        <div className="">
            <Navbar session={session} />
            <div className="flex flex-col min-h-svh items-center justify-center">
                <div>
                    <p className="text-base text-left">email: {session?.user?.email || ""}</p>
                    <p className="text-base text-left">fullname: {session?.fullname || ""}</p>
                </div>

            </div>
        </div>

    )
}
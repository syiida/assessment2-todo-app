"use client"
import { signIn } from 'next-auth/react';
import { useRouter } from 'next/navigation';
import React, { createContext, useMemo, useState } from 'react';
import Swal from 'sweetalert2'
let initialState: any = {
    actions: {
        handleLogin: (e: any) => { },
        setLoginType: (e: any) => { }
    }
};
const DataContext = createContext<typeof initialState>(initialState);
export function Provider(p: Readonly<React.PropsWithChildren>) {
    const [loginType, setLoginType] = useState("user")
    const router = useRouter()

    const handleLoginPassword = async (e: any) => {
        e.preventDefault()
        try {
            const response = await signIn('credentials', {
                redirect: false,
                email: e.target.email.value,
                password: e.target.password.value,
                type: "user",
                callbackUrl: '/task'
            });
            if (!response?.error) router.push('/task')
            else throw response.error
        } catch (error) {
            Swal.fire({
                title: 'Error',
                text: 'Pastikan email dan password yang kamu masukkan sudah benar',
                icon: 'error',
                confirmButtonText: 'Ok'
            })
        }
    }
    const handleLoginGuest = async (e: any) => {
        e.preventDefault()
        try {
            const response = await signIn('credentials', {
                redirect: false,
                email: 'guest@gmail.com',
                password: 'password',
                type: "guest",
                callbackUrl: '/task'
            });
            if (!response?.error) router.push('/task')
            else throw response.error
        } catch (error) {
            Swal.fire({
                title: 'Error',
                text: 'Terdapat kesalahan, coba beberapa saat lagi',
                icon: 'error',
                confirmButtonText: 'Ok'
            })
        }
    }
    const handleLogin = async (e: any) => {
        if (loginType === 'user') await handleLoginPassword(e)
        else if (loginType === 'guest') await handleLoginGuest(e)
    }

    const contextValue = useMemo(() => ({
        action: {
            handleLogin,
            setLoginType
        },
    }), [loginType]);

    return (
        <DataContext.Provider value={contextValue}>
            {p.children}
        </DataContext.Provider>
    );

}

export const useData = (): typeof initialState => React.useContext(DataContext);

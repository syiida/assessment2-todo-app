'use client'
import Button from "@/components/button";
import Navbar from "@/components/navbar";
import TaskList from "@/components/task-list"
import { useData } from "./provider";

export default function Page() {
    const { state, action } = useData()
    return (
        <>
            <Navbar session={state.session} />
            <div className="flex flex-col min-h-full items-center justify-center p-10">
                <h1 className="text-2xl sm:text-5xl">Task Management</h1>
                <div className="w-full md:max-w-[580px]">
                    <form className="mb-2 ">
                        <label htmlFor="title" className="block mb-2 text-base font-normal text-black dark:text-white">Title</label>
                        <input
                            type="text"
                            id="title"
                            name="title"
                            value={state.todo.title}
                            onChange={action.handleChange}
                            className="w-full bg-gray-50 border border-gray-300 text-black font-normal text-base rounded-lg focus:ring-blue-500 focus:border-blue-500 block p-2 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="title" required />

                        <div className="flex justify-center gap-2 py-4">
                            <Button className="bg-blue-button" label="Add Task" hidden={state.mode != 'new'} onClick={action.createTodo} />
                            <Button className="bg-orange-button" label="Update Task" hidden={state.mode == 'new'} onClick={action.updateTodo} />
                            <Button className="bg-red-button" label="Cancel" hidden={state.mode == 'new'} onClick={action.onCancelUpdate} />
                        </div>
                    </form>
                    <TaskList title="Ongoing Task" todos={state.incompleted_todos || []} />
                    <TaskList title="Completed Task" todos={state.completed_todos || []} />
                </div>
            </div>
        </>


    )
}
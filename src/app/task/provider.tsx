"use client"
import { Task } from '@prisma/client';
import { useSession } from 'next-auth/react';
import React, { createContext, useEffect, useMemo, useState } from 'react';
import Swal from 'sweetalert2'
let initialState: any = {
    state: {
        incompleted_todos: [],
        completed_todos: [],
        mode: "new",
        todo: {}
    },
    action: {
    }
};

const DataContext = createContext<typeof initialState>(initialState);

export function Provider(p: Readonly<React.PropsWithChildren>) {
    const [mode, setMode] = useState<string>(initialState.state.mode)
    const [completed_todos, setCompleted_todos] = useState<Task[]>(initialState.completed_todos)
    const [incompleted_todos, setIncompleted_todos] = useState(initialState.incompleted_todos)
    const [todo, setTodo] = useState<Task | any>({})
    const { data: session }: any = useSession()

    const getSortedDate = (data: Task[]): Task[] => {
        const sortedData = data.toSorted((a, b) => {
            return new Date(b.updated_at as Date).getTime() - new Date(a.updated_at as Date).getTime()
        })
        return sortedData
    }
    const getASCSortedDate = (data: Task[]): Task[] => {
        const sortedData = data.toSorted((a, b) => {
            return new Date(a.updated_at as Date).getTime() - new Date(b.updated_at as Date).getTime()
        })
        return sortedData
    }
    const getTodos = async () => {
        const response = await fetch(`/task/api`, {
            method: "POST",
            cache: 'no-store',
            body: JSON.stringify({
                query: "getData",
                bodyData: {
                    userId: session?.userId
                }
            })
        })
        const responseJson = await response.json()
        const todos = responseJson.data
        setCompleted_todos(getSortedDate(todos.filter((todo: Task) => todo.isdone === true)))
        setIncompleted_todos(getASCSortedDate(todos.filter((todo: Task) => todo.isdone === false)))

    }
    const handleChange = (event: any) => {
        let value = event.target.value;
        setTodo({
            ...todo,
            title: value
        })
    };
    const onCancelUpdate = () => {
        setTodo({})
        setMode(initialState.state.mode)
    }
    const getDetailTodo = async (id: any) => {
        setMode("edit")
        const response = await fetch(`/task/api`, {
            method: "POST",
            cache: 'no-store',
            body: JSON.stringify({
                query: "getDetail",
                bodyData: {
                    id: id
                }
            })
        })
        const responseJson = await response.json()
        const todo = responseJson.data
        setTodo(todo)
    }
    const updateTodo = async () => {
        const response = await fetch(`/task/api`, {
            method: "PATCH",
            cache: 'no-store',
            body: JSON.stringify({
                id: todo.id,
                title: todo.title,
                query: "update"
            })
        })
        const responseJson = await response.json()
        if (responseJson.status == 200) {
            Swal.fire({
                title: 'Success',
                text: 'Kamu berhasil memperbarui Task',
                icon: 'success',
                confirmButtonText: 'Ok'
            })
            getTodos()
        } else {
            Swal.fire({
                title: 'Gagal',
                text: 'Kamu Gagal memperbarui Task',
                icon: 'error',
                confirmButtonText: 'Ok'
            })
        }

    }
    const toggleTodo = async (todo: Task) => {
        const response = await fetch(`/task/api`, {
            method: "PATCH",
            cache: 'no-store',
            body: JSON.stringify({
                id: todo.id,
                isdone: !todo.isdone,
                query: "toggle"
            })
        })
        const responseJson = await response.json()
        if (responseJson.status == 200) {
            Swal.fire({
                title: 'Success',
                text: `Task berhasil dipindahkan ke ${todo.isdone ? 'Ongoing Task' : 'Completed Task'}`,
                icon: 'success',
                confirmButtonText: 'Ok'
            })
            getTodos()
        } else {
            Swal.fire({
                title: 'Gagal',
                text: 'Kamu Gagal memperbarui Task',
                icon: 'error',
                confirmButtonText: 'Ok'
            })
        }
    }
    const deleteTodo = async (id: any) => {
        const response = await fetch(`/task/api`, {
            method: "DELETE",
            cache: 'no-store',
            body: JSON.stringify({
                id: id
            })
        })
        const responseJson = await response.json()
        if (responseJson.status == 200) {
            Swal.fire({
                title: 'Success',
                text: `Task berhasil dihapus`,
                icon: 'success',
                confirmButtonText: 'Ok'
            })
            getTodos()
        } else {
            Swal.fire({
                title: 'Gagal',
                text: 'Kamu Gagal menghapus Task',
                icon: 'error',
                confirmButtonText: 'Ok'
            })
        }
    }
    const createTodo = async () => {
        const response = await fetch(`/task/api`, {
            method: "POST",
            cache: 'no-store',
            body: JSON.stringify({
                query: "create",
                bodyData: {
                    title: todo.title,
                    userId: session?.userId
                }
            })
        })
        const responseJson = await response.json()
        if (responseJson.status == 200) {
            Swal.fire({
                title: 'Success',
                text: `Berhasil menambahkan task`,
                icon: 'success',
                confirmButtonText: 'Ok'
            })
            getTodos()
        } else {
            Swal.fire({
                title: 'Gagal',
                text: 'Kamu gagal menambahkan task',
                icon: 'error',
                confirmButtonText: 'Ok'
            })
        }
    }

    useEffect(() => {
        if (session) getTodos()
    }, [session])
    const contextValue = useMemo(() => ({
        action: {
            updateTodo,
            onCancelUpdate,
            getDetailTodo,
            handleChange,
            toggleTodo,
            deleteTodo,
            createTodo
        },
        state: {
            mode: mode,
            completed_todos: completed_todos,
            incompleted_todos: incompleted_todos,
            todo: todo,
            session
        },
        hooks: {

        }
    }), [completed_todos, incompleted_todos, todo]);

    return (
        <DataContext.Provider value={contextValue}>
            {p.children}
        </DataContext.Provider>
    );

}

export const useData = (): typeof initialState => React.useContext(DataContext);

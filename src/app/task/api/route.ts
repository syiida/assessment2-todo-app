import prisma from "@/libs/prisma";
import { NextResponse } from "next/server"

export async function POST(request: Request) {
    const body = await request.json()
    const { query, bodyData } = body
    let data;
    try {
        switch (query) {
            case 'getData':
                const todos = await prisma?.task.findMany({
                    where: {
                        userId: bodyData.userId
                    }
                })
                if (todos) data = todos
                else throw todos
                break;
            case 'getDetail':
                const todo = await prisma.task.findFirst({
                    where: {
                        id: Number(bodyData.id)
                    }
                })
                if (todo) data = todo
                else throw todo
                break;
            case 'create':
                const newTodo = await prisma.task.create({
                    data: {
                        title: bodyData.title,
                        userId: Number(bodyData.userId)
                    }
                })
                if (newTodo) data = newTodo
                else throw newTodo
                break;
            default:
                break;
        }
        return NextResponse.json({ status: 200, error: false, data })
    } catch (error) {
        return NextResponse.json({ status: 500, error, data: null })
    }
}
export const PATCH = async (request: Request) => {
    const body = await request.json();
    const { id, title, query, isdone } = body
    try {
        let todo;
        switch (query) {
            case 'update':
                todo = await prisma.task.update({
                    where: {
                        id: Number(id)
                    },
                    data: {
                        title: title,
                        updated_at: new Date()
                    }
                });
                break;
            case 'toggle':
                todo = await prisma.task.update({
                    where: {
                        id: Number(id)
                    },
                    data: {
                        isdone: isdone,
                        updated_at: new Date()
                    }
                });
                break;
            default:
                break;
        }
        return NextResponse.json({ status: 200, data: todo });
    } catch (error) {
        return NextResponse.json({ status: 400, error });
    }

}
export const DELETE = async (request: Request) => {
    const body = await request.json();
    const { id } = body
    try {
        const user = await prisma.task.delete({
            where: {
                id: Number(id)
            }
        });
        return NextResponse.json({ status: 200, data: user });
    } catch (error) {
        return NextResponse.json({ statu: 400, error })
    }

}

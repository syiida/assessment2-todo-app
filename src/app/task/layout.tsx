'use client'
import React, { Suspense } from "react"
import { Provider } from "./provider"
import { SessionProvider } from "next-auth/react"
const Layout = (p: any) => {
    return (
        <Suspense>
            <SessionProvider>
                <Provider>
                    {p.children}
                </Provider>
            </SessionProvider>
        </Suspense>
    )
}

export default Layout
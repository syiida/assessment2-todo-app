import type { Metadata } from "next";
import { Poppins } from "next/font/google";
import "./globals.css";
import { Suspense } from "react";
import { Provider } from "./provider";

const poppins = Poppins({
  weight: ["100", "200", "300", "400", "500", "600", "700", "800", "900"], subsets: ["latin"]
});

export const metadata: Metadata = {
  title: "TodoApp-Rosyiidah Hasnaa",
  description: "Created By Rosyiidah Hasnaa",
  icons: './favicon.ico'
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body className={`bg-white ${poppins.className}`}>
        <Suspense>
          <Provider>
            {children}
          </Provider>
        </Suspense>
      </body>
    </html>
  );
}

import prisma from "@/libs/prisma"
export async function isEmailRegistered({ email }: { email: string }) {
    const isEmailRegistered = await prisma.user.findFirst({
        where: {
            email: email
        }
    })
    if (isEmailRegistered === null) return false
    else return true
}
export async function login({ email }: { email: string }) {
    try {
        const isLogin = await prisma.user.findFirst({
            where:
            {
                email: email
            }
        })
        if (isLogin === null) {
            return {
                status: 400,
                message: 'Email not registered'
            }
        } else {
            return {
                status: 200,
                user: isLogin
            }
        }
    } catch (error) {
        return {
            status: 400,
            message: error
        }
    }
}
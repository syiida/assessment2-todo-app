import { NextAuthOptions } from "next-auth";
import NextAuth from "next-auth/next";
import CredentialsProvider from "next-auth/providers/credentials";
import { login } from "../service";
const authOptions: NextAuthOptions = {
    session: {
        strategy: 'jwt'
    },
    secret: process.env.NEXTAUTH_SECRET,
    providers: [
        CredentialsProvider({
            type: 'credentials',
            name: 'Credentials',
            credentials: {
                email: { label: 'Email', type: 'email' },
                password: { label: 'Password', type: 'password' },
            },
            async authorize(credentials) {
                const { email, password, type } = credentials as {
                    email: string;
                    password: string
                    type: string
                }
                let user: any = {}
                const response = (await login({ email: email }))
                if (response.status === 200) {
                    user = { ...response.user }
                    if (password === user.password) return user
                } return null
            }
        })
    ],
    callbacks: {
        async jwt({ token, user, account }: any) {
            if (account?.provider === 'credentials') {
                token.email = user.email;
                token.fullname = user.fullname;
                token.type = user.type
                token.userId = user.id
            }
            return token
        },
        async session({ session, token }: any) {
            if ("email" in token) session.user.email = token.email
            if ("fullname" in token) session.fullname = token.fullname
            if ("type" in token) session.type = token.type
            if ("userId" in token) session.userId = token.userId
            return session
        }
    },
    pages: {
        signIn: '/'
    }
}

const handler = NextAuth(authOptions)
export {
    handler as GET, handler as POST
}
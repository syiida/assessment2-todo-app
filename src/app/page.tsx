'use client'

import LoginForm from "@/components/login-form"
import { useState } from "react";
import { useData } from "./provider";

export default function Home() {
  const [isVisible, setIsVisible] = useState(false)
  const { action } = useData()
  return (
    <main className="flex min-h-screen flex-col items-center justify-center p-24">
      <div className="border border-black p-8 rounded-[10px] min-h-[369px] min-w-[378px]">
        <div className="text-center mb-6">
          <div className="inline-flex items-center rounded-md shadow-sm" role="group">
            <button
              className="px-4 py-1 font-medium text-gray-900 bg-white border border-black rounded-s-[10px] hover:bg-blue-button  focus:z-10 focus:ring-2   focus:bg-blue-button"
              onClick={() => {
                action.setLoginType("user")
                setIsVisible(false)
              }}>
              User
            </button>
            <button
              className="px-4 py-1  font-medium text-gray-900 bg-white border rounded-e-[10px] border-black hover:bg-blue-button  focus:z-10 focus:ring-2  focus:bg-blue-button"
              onClick={() => {
                action.setLoginType("guest")
                setIsVisible(true)
              }}>
              Guest
            </button>
          </div>
        </div>

        <LoginForm isVisible={isVisible} />

      </div>
    </main>
  );
}

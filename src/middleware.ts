import { getToken } from 'next-auth/jwt'
import { NextResponse } from 'next/server'
import type { NextFetchEvent, NextMiddleware, NextRequest } from 'next/server'
const authPage = ['/']


function withAuth(middleware: NextMiddleware, requireAuth: string[]) {
    return async (req: NextRequest, next: NextFetchEvent) => {
        const pathname = req.nextUrl.pathname
        if (requireAuth.includes(pathname)) {
            const token = await getToken({
                req,
                secret: process.env.NEXTAUTH_SECRET
            })
            if (!token && !authPage.includes(pathname)) {
                const url = new URL('/', req.url)
                url.searchParams.set("callbackUrl", encodeURI(req.url))
                return NextResponse.redirect(url)
            }
            if (token) {
                if (authPage.includes(pathname)) {
                    return NextResponse.redirect(new URL('/task', req.url))
                }
                if (pathname.includes('/profile') && token.type === 'guest') {
                    return NextResponse.redirect(new URL('/task', req.url))
                }
            }
        }
        return middleware(req, next)
    }
}

export function mainMiddleware(request: NextRequest) {
    const res = NextResponse.next()
    return res
}
export default withAuth(mainMiddleware, ['/', '/task', '/profile', '/task/api', '/profile/api'])
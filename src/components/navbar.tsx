'use client'
import Link from "next/link"
import Button from "./button"
import { signOut } from "next-auth/react"
import { useState } from "react"

const Navbar = ({ session }: { session: any }) => {
    const [hidden, setHidden] = useState(true)
    return (
        <div>
            <div className="max-w-screen-xl flex flex-wrap items-center  mx-auto p-4">
                <button
                    onClick={() => {
                        setHidden(!hidden)
                    }}
                    data-collapse-toggle="navbar-default"
                    type="button"
                    className="inline-flex items-center p-2 w-10 h-10 justify-center text-sm text-gray-500 rounded-lg md:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600" aria-controls="navbar-default" aria-expanded="false">
                    <span className="sr-only">Open main menu</span>
                    <svg className="w-5 h-5" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 17 14">
                        <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M1 1h15M1 7h15M1 13h15" />
                    </svg>
                </button>
            </div>
            <nav className={`max-sm:${hidden ? 'hidden' : 'visible'}`} id="navbar-default">
                <div className="flex flex-col-reverse gap-2 md:flex-row md:justify-between py-3 px-8 border-b border-black" >
                    <div className="flex items-center">
                        <Button hidden={false} label="SignOut" className="bg-red-button hover:text-white" onClick={() => {
                            signOut()
                        }} />
                    </div>
                    <ul className="flex flex-col md:flex-row md:inline-flex md:justify-between gap-2">
                        <li><Link href="/task" className="text-[32px] hover:text-blue-700">Task</Link></li>
                        {session?.type === 'user' && <li><Link href="/profile" className="text-[32px] hover:text-blue-700">Profile </Link></li>}
                    </ul>
                    <div className="flex items-center">
                        <p className="max-sm:text-[32px] text-4xl">{session?.type === 'guest' ? 'Guest' : session?.fullname}</p>
                    </div>
                </div>
            </nav>

        </div>
    )
}

export default Navbar
const Button = ({ label, className, onClick, hidden }: { label: string; className?: string; onClick?: any, hidden: boolean }) => {
    return (
        <button className={`py-1 px-2 rounded-[10px] ${className}`} onClick={onClick} hidden={hidden}>{label}</button>
    )
}
export default Button
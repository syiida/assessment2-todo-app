import { useData } from "@/app/provider"

const LoginForm = ({ isVisible }: { isVisible: boolean }) => {
    const { action } = useData()

    return (
        <form className="" onSubmit={action.handleLogin}>
            <div className="mb-6" hidden={isVisible}>
                <div className="mb-2">
                    <label htmlFor="email" className="block mb-2 text-base font-normal text-black dark:text-white">Email</label>
                    <input type="email" id="email" name="email" className="bg-gray-50 border border-gray-300 text-black font-normal text-base rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full px-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Email" defaultValue={"hasnaa@gmail.com"} />
                </div>
                <div className="mb-2">
                    <label htmlFor="password" className="block mb-2 text-base font-normal text-black dark:text-white">Password</label>
                    <input type="password" id="password" name="password" className="bg-gray-50 border border-gray-300 text-black font-normal text-base rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full px-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Password" defaultValue={"password"} />
                </div>
            </div>
            <div className="text-center">
                <button type="submit" className="bg-green-button text-white rounded-[10px] py-1.5 px-4 border border-black focus:ring-2">Login</button>
            </div>
        </form>
    )
}
export default LoginForm
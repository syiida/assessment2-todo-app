import Card from "./card"
import NoData from "./no-data"

const TaskList = ({ title, todos }: { title: string; todos: any[] }) => {
    return (
        <div className="text-left my-3">
            <h2 className="font-bold text-base mb-3">{title}</h2>
            <div className="flex flex-col gap-2">
                {todos.length === 0 && <NoData />}
                {todos.map((todo, index: number) => <Card todo={todo} key={index} />)}
            </div>

        </div>
    )
}
export default TaskList
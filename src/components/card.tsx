import { Task } from "@prisma/client"
import { showFormattedDateTime } from "@/libs/showFormattedDateTime"
import { Pencil, X } from 'lucide-react';
import { useData } from "@/app/task/provider";

const Card = ({ todo }: { todo: Task }) => {
    const { action } = useData()
    return (
        <div className="bg-gray px-2 py-3 rounded-[10px] flex justify-between items-center">
            <div>
                <p>{todo.title}<button onClick={() => {
                    action.getDetailTodo(todo.id)
                }}><Pencil size={14} className="ml-2" /></button></p>
                <p className="text-xs">{showFormattedDateTime(todo.updated_at as Date)}</p>
            </div>
            <div className="flex gap-2">
                <button
                    onClick={() => action.deleteTodo(todo.id)}
                    className="w-6 h-6 rounded-full border border-black focus:text-red-button focus:border-red-button focus:ring-2 focus:ring-red-button">
                    <X />
                </button>
                <input className="w-6 h-6 rounded-full" checked={todo.isdone} type="checkbox" value={String(todo.isdone)} onChange={(e) => {
                    e.target.value = String(!e.target.checked)
                    e.target.checked = !e.target.checked
                    action.toggleTodo(todo)
                }} />
            </div>

        </div>
    )
}
export default Card
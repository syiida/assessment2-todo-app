const NoData = () => {
    return (
        <div className="bg-gray px-2 py-3 rounded-[10px] flex justify-between items-center">
            <p>---No Task---</p>
        </div>
    )
}
export default NoData
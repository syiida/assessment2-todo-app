const showFormattedDate = (date: Date) => {
    return new Date(date).toLocaleDateString("id", {
        day: 'numeric',
        month: 'short',
        year: "numeric",
    });
};

const showFormattedTime = (time: Date) => {
    return new Date(time).toLocaleTimeString('id', {
        hour: "2-digit",
        minute: "2-digit",
        timeZone: "Asia/Jakarta"
    })
}

const showFormattedDateTime = (date: Date) => {
    return showFormattedDate(date) + " " + showFormattedTime(date)
}
export { showFormattedDate, showFormattedTime, showFormattedDateTime };
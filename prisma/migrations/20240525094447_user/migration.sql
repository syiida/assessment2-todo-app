/*
  Warnings:

  - A unique constraint covering the columns `[type]` on the table `User` will be added. If there are existing duplicate values, this will fail.
  - Added the required column `type` to the `User` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "User" ADD COLUMN     "type" VARCHAR(255) NOT NULL;

-- CreateIndex
CREATE UNIQUE INDEX "User_type_key" ON "User"("type");

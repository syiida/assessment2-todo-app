import { PrismaClient } from '@prisma/client'
const prisma = new PrismaClient()
async function main() {
    await prisma.user.upsert({
        update: {},
        where: {
            email: "guest@gmail.com"
        },
        create: {
            email: 'guest@gmail.com',
            fullname: 'Guest',
            password: "password",
            type: "guest",
            Tasks: {
                create: [
                    { title: 'Tambahkan Komponen Logout pada navbar' },
                    { title: 'Task from guest' }
                ],
            },
        },
    })
    await prisma.user.upsert({
        update: {},
        where: {
            email: "hasnaa@gmail.com"
        },
        create: {
            email: 'hasnaa@gmail.com',
            fullname: 'Rosyiidah Hasnaa',
            password: "password",
            type: "user",
            Tasks: {
                create: [
                    { title: 'Tambahkan Komponen Logout pada navbar User' },
                    { title: 'Task from User' }
                ],
            },
        },
    })
}
main()
    .then(async () => {
        await prisma.$disconnect()
    })
    .catch(async (e) => {
        console.error(e)
        await prisma.$disconnect()
        process.exit(1)
    })
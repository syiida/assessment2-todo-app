import type { Config } from "tailwindcss";

const config: Config = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
    "./node_modules/flowbite/**/*.js"
  ],
  theme: {
    extend: {
      backgroundImage: {
        "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
        "gradient-conic":
          "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
      },
      colors: {
        'blue-button': '#6FCBFF',
        'green-button': '#48AA52',
        'orange-button': '#FFB46F',
        'red-button': '#FF6F6F',
        'gray': "#D0D0D0"
      },
    },
  },
  plugins: [
    require('flowbite/plugin')
  ]

};
export default config;

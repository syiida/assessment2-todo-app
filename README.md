## Cara menjalannkan project

- Install seluruh depedency yang dibutuhkan

```bash
yarn install
```

- Copy file .env.sample ke file .env dan sesuaikan dengan database local

```bash
cp .env.sample .env
```
- Migrasi kebutuhan database

```bash
npx prisma generate
npx prisma migrate dev
```
- Jalankan seeder untuk inisiasi nilai awal tabel user pada database

```bash
npx prisma db seed
```
- Jalannkan project dengan perintah

```bash
yarn dev
```